import type { CellSaveState, ILoadingCellsEvent } from './engine';
import { Cell, GameEngine } from './engine'
import { GameData } from './models/game'
import Parser from './parser/parser'
import type { IGraphJson } from './parser/serializer';
import Serializer from './parser/serializer'
import BaseUI from './ui/base'
import type {
    Cellish,
    CellishJson,
    Engineish,
    GameEngineHandler,
    GameEngineHandlerOptional,
    Optional,
    PuzzlescriptWorker,
    TypedMessageEvent,
    WorkerMessage,
    WorkerResponse } from './util';
import {
    _debounce,
    _flatten,
    EmptyGameEngineHandler,
    INPUT_BUTTON,
    MESSAGE_TYPE,
    pollingPromise,
    RULE_DIRECTION,
    shouldTick,
    spritesThatInteractWithPlayer } from './util'

// Public API
export type { ILoadingCellsEvent, Optional };
export { Parser, GameEngine, Cell, GameData, RULE_DIRECTION, BaseUI, Serializer }

// Semi-public API (used by the CLI)
import { logger } from './logger'
import { CollisionLayer } from './models/collisionLayer'
import type { IColor } from './models/colors';
import { HexColor } from './models/colors'
import { Dimension } from './models/metadata'
import type { A11Y_MESSAGE} from './models/rule';
import { A11Y_MESSAGE_TYPE } from './models/rule'
import { GameSprite } from './models/tile'
import type { Soundish } from './parser/astTypes';
import { LEVEL_TYPE } from './parser/astTypes'
import { saveCoverageFile } from './recordCoverage'
import TableUI from './ui/table'

export { logger, LEVEL_TYPE, saveCoverageFile, _flatten, EmptyGameEngineHandler, INPUT_BUTTON }
export type { IColor, Soundish, Cellish, GameEngineHandler };
export { _debounce, CollisionLayer, GameSprite, spritesThatInteractWithPlayer }
export type { CellSaveState, A11Y_MESSAGE };
export { A11Y_MESSAGE_TYPE }
export type { CellishJson, TypedMessageEvent, WorkerMessage, WorkerResponse };
export { MESSAGE_TYPE, pollingPromise, shouldTick }
export { TableUI }
export type { Engineish, GameEngineHandlerOptional }
export type { IGraphJson, PuzzlescriptWorker };
export { Dimension, HexColor }
