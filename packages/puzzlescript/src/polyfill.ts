export const NODE_ENV = globalThis?.process?.env?.NODE_ENV ?? 'production'
export const LOG_LEVEL = globalThis?.process?.env?.LOG_LEVEL
export const VERIFY_MATCHES = globalThis?.process?.env?.VERIFY_MATCHES
export const PUZZLESCRIPT_METHOD = globalThis?.process?.env?.PUZZLESCRIPT_METHOD
