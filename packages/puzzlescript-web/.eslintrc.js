module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "node": true
    },
    "extends": [
        // "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        // 'plugin:@typescript-eslint/recommended-requiring-type-checking',
    ],
    "overrides": [
    ],
    "parser": "@typescript-eslint/parser",
    parserOptions: {
        tsconfigRootDir: __dirname,
        project: ['./tsconfig.json'],
    },
    "plugins": [
        "@typescript-eslint"
    ],
    "rules": {
        '@typescript-eslint/consistent-type-imports': "error",
        '@typescript-eslint/consistent-type-exports': "error",
        '@typescript-eslint/no-var-requires': "error",
        //"indent": ["error", 4]
    }
}
