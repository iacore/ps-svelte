import SyncTableEngine from './browser/SyncTableEngine'
import WebworkerTableEngine from './browser/WebworkerTableEngine'

export default {
    WebworkerTableEngine,
    SyncTableEngine
}

export {
    WebworkerTableEngine,
    SyncTableEngine
}